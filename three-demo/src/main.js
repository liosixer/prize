import  { createApp } from 'vue'
import App from './App.vue'

console.log('当前开发环境：', process.env.NODE_ENV);
// const { } = process.NODE_ENV;
// 若是没有开启Devtools工具，在开发环境中开启，在生产环境中关闭
// if (process.env.NODE_ENV == 'development') {
//   Vue.config.devtools = true;
// } else {
//   Vue.config.devtools = false;
// }

createApp(App).mount('#app')


