//用canvas生成图片
import * as THREE from "three";
import _ from "lodash";

const createSpriteByCanvas = (params) => {
  const { content, font } = params;
  let canvas = document.createElement("canvas");
  let ctx = canvas.getContext("2d");
  canvas.width = 300;
  canvas.height = 100;
  //制作矩形
  ctx.fillStyle = "rgba(255,165,0,0.8)";
  ctx.fillRect(0, 0, 300, 300);
  //设置文字
  ctx.fillStyle = "#fff";
  ctx.font = 'normal 18pt "楷体"';
  ctx.fillText("标签", 100, 20);
  let textWord = _.truncate(content, { length: 10 });
  ctx.fillText(textWord, 15, 60);
  //生成图片
  let url = canvas.toDataURL("image/png");

  //将图片构建到纹理中
  let geometry1 = new THREE.PlaneGeometry(30, 30);
  const texture = new THREE.TextureLoader().load(url);
  // Cannot read properties of undefined (reading 'elements')
  // 用let创建会报错？

  let material1 = new THREE.MeshBasicMaterial({
    map: texture,
    side: THREE.DoubleSide,
    opacity: 1,
    transparent: true,
  });

  let rect = new THREE.Mesh(geometry1, material1);
  rect.name = content;
  // rect.position.set(1 * Math.random(), 1, 1);
  // scene.add(rect)
  console.log('创建矩形块', rect);
  return rect;

};

export { createSpriteByCanvas };
